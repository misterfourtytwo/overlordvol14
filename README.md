# Fan translation of Overlord Volume 14

## Download

[Latest versions](https://gitlab.com/toborage/overlordvol14/-/releases)

## Official releases

**Note:** Please support the author and the series by purchasing the official works.

Japanese - [Kadokawa](https://www.kadokawa.co.jp/product/321910000095/) - Official

English - [Yen Press](https://yenpress.com/series-search/?series=overlord&supapress_order=publishdate-asc) - Official

Polish - [Kotori](http://kotori.pl/nasze-wydania/overlord/) - Official

Portuguese - [JBC](https://mangasjbc.com.br/titulos/overlord/) - Official

Spanish - [Panini](http://comics.panini.com.mx/store/pub_mex_es/catalogsearch/advanced/result/?name=overlord&p=1&pnn_anno_pubblicazione%5Bfrom%5D=&pnn_anno_pubblicazione%5Bto%5D=&price%5Bfrom%5D=&price%5Bto%5D=&sku=) - Official
